<?php

namespace App\Controller;

use App\Entity\Partie;
use App\Repository\PartieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(EntityManagerInterface $entityManager, PartieRepository $partieRepository): Response
    {
        $bestParties = $partieRepository->findBy([], ['score' => 'DESC']); // Récupérer toutes les parties classées par score

        $user = $this->getUser();
        $userScore = 0;
        $userRank = null;

        if ($user) {
            $userScore = $partieRepository->getScoreByUser($user); 
            $userRank = $this->getUserRank($userScore, $bestParties); 
        }

        return $this->render('home/index.html.twig', [
            'bestParties' => $bestParties,
            'userScore' => $userScore,
            'userRank' => $userRank,
        ]);
    }

    private function getUserRank(int $userScore, array $bestParties): ?int
    {
        $rank = null;

        foreach ($bestParties as $key => $partie) {
            if ($partie->getScore() <= $userScore) {
                $rank = $key + 1;
                break;
            }
        }

        return $rank;
    }
}
