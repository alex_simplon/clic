<?php

namespace App\Controller;

use App\Entity\Partie;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PartieController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/partie', name: 'app_partie')]
    public function index(): Response
    {
        return $this->render('partie/index.html.twig');
    }

    #[Route('/increment-click-count', name: 'app_increment_click_count', methods: ['POST'])]
    public function incrementClickCount(Request $request): Response
    {

        $score = $request->getSession()->get('score', 0);
        $score++;
        $request->getSession()->set('score', $score);

        return new Response();
    }

    #[Route('/end-game', name: 'app_end_game')]
    public function endGame(Request $request, EntityManagerInterface $entityManager): Response
    {

        $score = $request->getSession()->get('score', 0);


        $user = $this->getUser();


        $partie = new Partie();
        $partie->setScore($score);
        $partie->setUser($user);
        $partie->setCreatedAt(new \DateTimeImmutable());

        $entityManager->persist($partie);
        $entityManager->flush();


        return $this->redirectToRoute('app_resultat', ['score' => $score]);
    }

    #[Route('/partie/resultat', name: 'app_resultat')]
    public function resultat(Request $request): Response
    {

        $score = $request->getSession()->get('score', 0);


        $request->getSession()->set('score', 0);


        $rank = $this->calculateUserRank($score);

        return $this->render('partie/resultat.html.twig', [
            'score' => $score,
            'rank' => $rank,
        ]);
    }

    private function calculateUserRank(int $score): ?int
    {

        $repository = $this->entityManager->getRepository(Partie::class);
        $qb = $repository->createQueryBuilder('p');
        $rank = $qb
            ->select('COUNT(p.id) + 1')
            ->where('p.score > :score')
            ->setParameter('score', $score)
            ->getQuery()
            ->getSingleScalarResult();

        return $rank;
    }
}
